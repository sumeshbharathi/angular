import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthComponent } from "./auth/auth.component";
import { HomeComponent } from "./home/home.component";
import { NavComponent } from "./nav/nav.component";

const routes: Routes = [
  { path: "auth", component: AuthComponent },
  { path: "home", component: HomeComponent },
  { path: "", component: NavComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
